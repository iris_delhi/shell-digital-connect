<?php
// Loads useragent.php from child or parent theme
require_once('useragent/useragent.php');

/*
 * Get current user ip address
 *
 */
function whatsMyIP()
{
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
    {
        if (array_key_exists($key, $_SERVER) === true)
        {
            foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip)
            {
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
                {
                    return $ip;
                }
            }
        }
    }
}

/*
 * Create table for visitors
 *
 */ 
add_action("after_switch_theme", "shell_create_visitor_tbl");
function shell_create_visitor_tbl(){
	global	$wpdb;
	$tblname	=	$wpdb->prefix.'visitor';
	$charset_collate = $wpdb->get_charset_collate();
	
	$visitor_sql = "CREATE TABLE IF NOT EXISTS $tblname (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time_stamp VARCHAR(100),
		ipaddress VARCHAR(100),
		email VARCHAR(100),
		user_agent text ,
		visitor_browser VARCHAR(255),
		visitor_platform VARCHAR(100),
		PRIMARY KEY  (id)
	) $charset_collate;";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $visitor_sql );
}

/*
 * Submit Data for unknown ip address 
 *
 */
add_action('init', 'shell_verifyUnknownUser');
function shell_verifyUnknownUser(){
	if(isset( $_POST['checkValidShellEmp'] )){
		$validshellemp 	=	$_POST['checkValidShellEmp'];
	}else{
		$validshellemp 	=	'';
	}
	if ( $validshellemp ||  wp_verify_nonce( $validshellemp, 'empValidShell' ) ) {
			global	$wpdb;
			$tblname	=	$wpdb->prefix.'visitor';
			$timestamp	=	time();
			$ipaddress	=	whatsMyIP();
			$useragent	=	serialize($_SERVER['HTTP_USER_AGENT']);
			
			$visitor_browser	=	getBrowser();
			$visitor_os			=	getOs();
			$visitor_device		=	getDevice();
			
			$emailaddress		=	trim($_POST['validShellEmail']);
			
			if($visitor_device == 'Other'){
				$visitorOsDevice	=	"Desktop - ".$visitor_os;
			}else{
				$visitorOsDevice	=	$visitor_device." - ".$visitor_os;				
			}
					
			$datainserted = $wpdb->query("INSERT INTO $tblname SET time_stamp = '".$timestamp."' , ipaddress='".$ipaddress."' , email='".$emailaddress."'  , user_agent='".$useragent."' , visitor_browser='".$visitor_browser."' ,visitor_platform='".$visitorOsDevice."'  ");
			if($datainserted){
				wp_redirect(site_url());
				exit;
			}
		
	}
}

?>