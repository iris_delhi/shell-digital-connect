<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="leadership">
  <div class="container">
    <div class="inner-content">
    <?php while ( have_posts() ) : the_post(); ?>
    	<?php the_content(); ?>
      <?php endwhile; ?>
    </div>
  </div>
</div>
<?php get_footer();

