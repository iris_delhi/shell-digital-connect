<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
$postformate = get_field('format');
if($postformate == 'section'):
?>
<section class="<?php echo get_field('extra_class'); ?> inner-content">
	<?php while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
    <?php endwhile; ?>
</section>
<?php else: ?>
<div class="single-post-detail <?php echo get_field('extra_class'); ?>">
  <div class="container">
    <div class="inner-section">
      <?php while ( have_posts() ) : the_post(); ?>
      	<?php the_content(); ?>
      <?php endwhile; ?>
    </div>
  </div>
</div>
<?php endif; ?>
<?php get_footer();



