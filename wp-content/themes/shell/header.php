<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108092322-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108092322-1');
</script>
</head>

<body <?php body_class(); ?>>
<div class="wraper">
    <div class="layer">
        <header class="dc-header">
			<?php get_template_part( 'template-parts/header/header', 'left' ); ?>
            <?php if ( has_nav_menu( 'primary' ) ) : ?>
               		<?php get_template_part( 'template-parts/header/site', 'navigation' ); ?>
            <?php endif; ?>
            <?php get_template_part( 'template-parts/header/header', 'right' ); ?>
            <div class="clr"></div>
        </header>
