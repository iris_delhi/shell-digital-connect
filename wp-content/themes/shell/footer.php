<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.2
 */

?>
<?php get_template_part( 'template-parts/footer/footer', 'popup' ); ?>
<footer class="shell-footer">
	<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
</footer>
<!-- #colophon -->
</div>
<!-- .site-content-contain -->
</div>
<!-- #page -->
<?php wp_footer(); ?>

</body></html>