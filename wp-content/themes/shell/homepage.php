<?php
/**
 * Template Name: Homepage 
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="home-content">
  <div class="clearfix"></div>
        <div class="container">
        <?php
        if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				$args = array(
					'posts_per_page'   => -1,
					'offset'           => 0,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'post',
					'post_status'      => 'publish',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'name',
							'terms' => 'homepage'
						)
					),
					'suppress_filters' => true 
				);
				$posts_array = get_posts( $args ); 
				$count	=	1;
				if(sizeof($posts_array) > 0):
					foreach($posts_array as $article):
						$featuredImgUrl = get_the_post_thumbnail_url($article->ID,'full'); 
						$class = ($count == 4) ? 'china-txt' :'countries';
						?>
						<a href="<?php echo get_permalink($article->ID); ?>" title="<?php echo get_the_title($article->ID); ?>">
                            <div class="tile<?php echo $count; ?>">
								<?php if($featuredImgUrl): ?>
                                	<img src="<?php echo $featuredImgUrl; ?>" alt="<?php echo get_the_title($article->ID); ?>">
                                <?php endif; ?>
                                <p class="<?php echo $class; ?>"><?php echo get_the_title($article->ID); ?></p>
                            </div>
						</a>
						<?php
						$count++;
					endforeach; //$posts_array as $article
				endif; //sizeof($posts_array) > 0
			endwhile; //while ( have_posts() ) : the_post();
        endif; //if ( have_posts() ) :
		?>
        </div>
</div>
<div class="clearfix"> </div>
<?php get_footer(); ?>
