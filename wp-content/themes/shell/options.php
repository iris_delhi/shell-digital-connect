<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'shell-digital-connect';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'shelldigitalconnect'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {


	$wpeditorsettings = array(
		'wpautop' => false, // Default
		'textarea_rows' => 20,
		'media_buttons' => true,
		'tinymce' => array( 'plugins' => 'wordpress,wplink' )
	);

	$options = array();
	
	$options[] = array(
		'name' => __( 'Basic Settings', 'shelldigitalconnect' ),
		'type' => 'heading'
	);
	
	$options[] = array(
		'name' => __( 'Digital Connect Logo', 'shelldigitalconnect' ),
		'id'   => 'sitelogo',
		'type' => 'upload'
	);
	
	$options[] = array(
		'name' => __( 'Shell Logo', 'shelldigitalconnect' ),
		'id'   => 'shelllogo',
		'type' => 'upload'
	);
	$options[] = array(
		'name' => __( 'Digital Connect URL', 'shelldigitalconnect' ),
		'id'   => 'digitalconnecturl',
		'type' => 'text'
	);	
	$options[] = array(
		'name' => __( 'Homepage Post Category ID', 'shelldigitalconnect' ),
		'id'   => 'homecatid',
		'type' => 'upload'
	);
		
	

	return $options;
}