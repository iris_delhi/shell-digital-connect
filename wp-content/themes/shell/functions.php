<?php
/**
 * Shell Digital Connect functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 */
if(!defined('FUSEURL')){
	define('FUSEURL',get_template_directory_uri() . '/');
}
if(!defined('FUSEPATH')){
	define('FUSEPATH',get_template_directory() . '/');
}
function pre($data){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function shelldigitalconnect_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/shelldigitalconnect
	 * If you're building a theme based on Shell Digital Connect, use a find and replace
	 * to change 'shelldigitalconnect' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'shelldigitalconnect' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	  register_nav_menu( 'primary', __( 'Primary Menu', 'shelldigitalconnect' ) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', shelldigitalconnect_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	


}
add_action( 'after_setup_theme', 'shelldigitalconnect_setup' );


/**
 * Register custom fonts.
 */
function shelldigitalconnect_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'shelldigitalconnect' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shelldigitalconnect_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'shelldigitalconnect' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your footer.', 'shelldigitalconnect' ),
		'before_widget' => '<div id="%1$s" class="footer-block %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'shelldigitalconnect_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Shell Digital Connect 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function shelldigitalconnect_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'shelldigitalconnect' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'shelldigitalconnect_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Shell Digital Connect 1.0
 */
function shelldigitalconnect_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'shelldigitalconnect_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function shelldigitalconnect_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'shelldigitalconnect_pingback_header' );



/**
 * Enqueue scripts and styles.
 */
function shelldigitalconnect_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'shelldigitalconnect-fonts', shelldigitalconnect_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'shelldigitalconnect-style', get_stylesheet_uri() );
	wp_enqueue_script('jquery');

	wp_enqueue_script( 'cookie', get_theme_file_uri( '/assets/js/js.cookie.js' ), array(), '2.1.4' );
	
	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );
	
	wp_enqueue_script( 'global', get_theme_file_uri( '/assets/js/global.js' ), array(), '1.0.0' );

}
add_action( 'wp_enqueue_scripts', 'shelldigitalconnect_scripts' );

if ( ! function_exists( 'shelldigitalconnect_edit_link' ) ) :
/**
 * Returns an accessibility-friendly link to edit a post or page.
 *
 * This also gives us a little context about what exactly we're editing
 * (post or page?) so that users understand a bit more where they are in terms
 * of the template hierarchy and their content. Helpful when/if the single-page
 * layout with multiple posts/pages shown gets confusing.
 */
function shelldigitalconnect_edit_link() {
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'shelldigitalconnect' ),
			get_the_title()
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/*
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */

define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

//Display the classes for the body element.
add_filter( 'body_class', 'shelldigitalconnect_body_class' );
function shelldigitalconnect_body_class( $classes ) {
	global $post;
	$postid 	= 	$post->id;
	$bodyclass 	= 	get_field( "body_class", $postid);
	$classes[]	=	$bodyclass;
	
	 if ( is_home() || is_front_page() ) {
        $classes[] = ' is_home is_front_page';
    }
	
    return $classes;
}

// include authorize file
require_once('authorize.php');




