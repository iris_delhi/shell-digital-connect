<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

?>
<?php dynamic_sidebar( 'sidebar-1'); ?>
