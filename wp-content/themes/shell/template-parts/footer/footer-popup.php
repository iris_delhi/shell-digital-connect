<div class="shellemailauth" style="display:none">
  <div class="shellemailauthbox">
    <div class="shellemailauthfrm">
      <div class="entry-inner-content">
        <header class="popup-entry-header">
          <h2 class="verify-content-title"> Share your email address to access the portal </h2>
        </header>
        <div class="entry-content">
          <div class="verify-user-box" id="verifyUserBox">
            <div class="verify-message">
              <div class="verify-content">
                <form method="post" name="checkValideEmp" id="checkValideEmp">
                <?php wp_nonce_field( 'empValidShell', 'checkValidShellEmp' ); ?>
                  <input type="text" class="emailbox" id="validShellEmail" name="validShellEmail" placeholder="Enter your email..">
                  <span id="validEmpEmailMsg" data-id="validShellEmail"> </span>
                  <input type="submit" class="submitBtn" value="Submit">
                </form>
              </div>
              <div class="clearfix"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.shellemailauth{
	display: block;
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.9);
    top: 0;
    left: 0;
    z-index: 99;
}
.shellemailauthbox{
	max-width: 710px;
    margin: 0 auto;
    position: relative;
    background: white;
    padding: 10px;
    box-sizing: border-box;
    padding-bottom: 50px;
	top: 30%;
}

.shellemailauthfrm .authemailbox {
	width: 100%;
    display: block;
    height: auto;
    padding-top: 10px;
}
.entry-inner h2.verify-content-title {
	color: #000;
	font-size: 20px;
	margin:0;
}
.shellemailauthfrm {
    padding: 10%;
}
.entry-inner-content{
    width: 100%;
    margin: 0 auto;
    text-align: center;
}
.verify-message .verify-content input[type="text"] {
    width: 260px;
}

.verify-message .verify-content input[type="text"], .verify-message .verify-content input[type="submit"] {
    background-color: #FFF;
    border-color: #4d545b;
    height: 46px;
    color: #858d95;
	border-radius:5px;
}
.verify-message .verify-content #validEmpEmailMsg {
    float: left;
    width: 100%;
    margin-top: 20px;
}
.verify-message .verify-content input[type="submit"] {
    background-color: #e7ae01;
    border-color: #e7ae01;
    color: white;
    width: 120px;
	margin-top:20px;
}
.verify-message .verify-content input#validShellEmail{
	padding:5px;
	height:35px;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(e) {
if(Cookies.get('validshellemp') == 'authemail'){
	jQuery('.shellemailauth').fadeOut('slow');
}else{
	jQuery('.shellemailauth').fadeIn('slow');
}	
   
jQuery(document).on('submit','form#checkValideEmp',function(e){
	var empEmail	=	jQuery('#validShellEmail').val();
	var emailDomain = 	empEmail.split('@').slice(1);
	var $flag		= 	false;
	
	if (empEmail.length == 0 || !validateEmail(empEmail)) {
		jQuery("#validEmpEmailMsg").html('Enter a valid shell email address !');
		e.preventDefault();
		$flag == false;
	}else if(emailDomain[0] == 'shell.com'){
		$flag = true;
	}else{
		jQuery("#validEmpEmailMsg").html('Enter a valid shell email address !');
		e.preventDefault();
		$flag = false;
	}
	if($flag == true ){
		Cookies.set('validshellemp', 'authemail', { expires: 7, path: '/' });
		return true;
	}else{
		return false;
	}

});

jQuery(document).on('focus','form#checkValideEmp input#validShellEmail',function(e){	
	var checkMessageBox =	jQuery(this).attr('id');
	jQuery('[data-id="'+checkMessageBox+'"]').html('');
});


function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}else {
		return false;
	}
}


});
</script>