<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'shelldigitalconnect' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'shelldigitalconnect' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
