<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

?>

<div class="shell-dc-logo"> 
    <a href="<?php echo esc_url( of_get_option('digitalconnecturl') ); ?>" rel="home" title="<?php echo get_option('blogname'); ?>"> 
    	<?php 
		$sitelogo = of_get_option('sitelogo');
		if($sitelogo):
			echo '<img src="'.$sitelogo.'" alt="'.get_option('blogname').'" class="site-logo"> ';
		else:
			echo get_option('blogname');
		endif;
		?>
    </a> 
</div>
