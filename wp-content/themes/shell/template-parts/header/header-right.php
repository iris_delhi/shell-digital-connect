<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

?>
<div class="shell-logo"> 
	<?php 
    $shelllogo = of_get_option('shelllogo');
    if($shelllogo):
    	echo '<img src="'.$shelllogo.'" alt="Shell" title="Shell">';
    endif;
    ?> 
</div>
