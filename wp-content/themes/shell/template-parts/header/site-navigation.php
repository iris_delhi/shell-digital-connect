<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.2
 */

wp_nav_menu( array(
    'menu'   => 'primary',
	'container' => 'nav', 
	'container_class' => 'dc-nav', 
	'container_id' => 'siteNavigationTop', 
	'menu_class' => 'nav-list', 
	'menu_id' => 'nav-list',
) );
 
?>
