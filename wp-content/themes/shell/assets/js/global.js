(function( $ ) {
	function addSubMenuClass(){
		$('ul.sub-menu').each(function(index, element) {
			if($(this).parents('ul.sub-menu').length > 0){
				$(this).addClass('nav-list-2-ul-2 sub-sub-menu');
			}else{
				$(this).addClass('nav-list-2 ul-list-2');
			}
            
        });
		$('img.menu-image.menu-image-title-above').each(function(index, element) {
            $(this).wrap('<div class="nav-icon"></div>');
			//$(this).removeClass('menu-image , menu-image-title-above');
			$(this).removeAttr('class');
			$(this).removeAttr('width');
			$(this).removeAttr('height');
			$(this).removeAttr('srcset');
			$(this).removeAttr('sizes');
        });
	}
	// Fire on document ready.
	$( document ).ready( function() {
		if ( $('#siteNavigationTop').length ) {
			addSubMenuClass();
		}
	});
	setTimeout(function(){
			$("p").each(function(){
				var self = $(this);
				// Trimming white space
				self.filter(function () { return $.trim(this.innerHTML) == "" }).remove();
				
				// Without trimming white space
				self.filter(function () { return this.innerHTML == "" }).remove();
				if ( self.html().trim().length == 0 ){
					self.remove();
				}
			});
	},500);
})( jQuery );


