<?php
/**
 * Template Name: Leadership
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Shell_Digital_Connect
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="leadership">
  <div class="container">
    <div class="inner-content">
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="right-col">
        <?php the_post_thumbnail('full', array('class' => 'img-responsive responsive--full image-pan')); ?>
        <span><?php the_field('featured_title');?></span> 
        </div>
      <div class="digital-text">
        <?php the_content(); ?>
      </div>
      <?php endwhile; ?>
    </div>
  </div>
</div>
<?php get_footer();

