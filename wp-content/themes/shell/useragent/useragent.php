<?php
require_once 'vendor/autoload.php';
use UAParser\Parser;

$ua = $_SERVER['HTTP_USER_AGENT'];

$parser = Parser::create();
$result = $parser->parse($ua);

function getBrowser(){
	global $result;
	return $result->ua->toString();
}
function getOs(){
	global $result;
	return $result->os->toString();
}
function getDevice(){
	global $result;
	return $result->device->family;
}
?>