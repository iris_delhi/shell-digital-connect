<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shelldigitalconnect');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c=Mdk9im`382.:PJJ(I#D@C*WF a]W[#rO*Or5)YWpL:V`|Do)Y9vn YBCr 1:QV');
define('SECURE_AUTH_KEY',  'JzY64K&U?I=7=T 1tU9;R_^lj wG%SF5|R2vQ ?i.[!{jAo2+@|D81~P|uCi<cBh');
define('LOGGED_IN_KEY',    ';%^Bo2Q/N,[O-X5}e6-(k;BM#zb3Qk${LQk1D5<!K!-un)px!bP$U7a: rGhfpcm');
define('NONCE_KEY',        'bw&x<~Af1&SxW.-9K.#?}ghjMsLrH,(|T6SfWN_taI2$S J`8EGkhz|SUtem[]%4');
define('AUTH_SALT',        '>s(<Cbz3@/I|e6R<e%%5b&o,sm}!aLGjBi^X$bUNH4KhWo6=!kI&FI(u*]A |s3*');
define('SECURE_AUTH_SALT', '!!Kf;1#bW@W`]545 j-f0ImpQ!R6U/nlL#^7p!!QQb1Q;)8+@:-$.Z5eQ+~Mtbzk');
define('LOGGED_IN_SALT',   '_Y43a}qZ+xR%R{M={zVN)y}NW?w/mh3LrLV~Fc`gb`aEV00 )rTuH~bcjG3+]@*2');
define('NONCE_SALT',       'od6S7>|>tskMSmBI2Hmj{C?+&})d*}z]wU<:Cp*gfNv(WtgBJ-V/Iv*NPez,8]$f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sdc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

## Disable Editing in Dashboard
define('DISALLOW_FILE_EDIT', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
